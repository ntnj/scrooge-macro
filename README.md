# Scrooge

Scrooge is a [thrift](https://thrift.apache.org/) code generator written in
Scala, which currently generates code for Scala and Java.

This project adds some scala macro-annotations to automatically generate methods
to encode/decode case classes to a codec compatible with scrooge. This uses the experimental
macro-paradise plugin for scala compiler to compile, but doesn't add any runtime dependencies.

Since, scala scrooge classes are traditionally generated by scala code-gen from
thrift files, it's impossible to currently add your own methods to the generated
files. If you need to do any operations on those classes, they then have to be defined twice,
once in thrift files, and once in model scala code. Instead, this project adds annotations
`@ThriftStruct` and `@ThriftField` to be added to scala code.

To use them,

    import com.twitter.scrooge.annotations._

    @ThriftStruct
    case class ABC(a: Int, b: Set[Long], c: Map[Short, Option[Boolean]])

This will modify the class `ABC` to extend `com.twitter.scrooge.ThriftStruct` and define
method `write` on it, and create a companion object `ABC`, if not present, extending
`ThriftStructCodec[ABC]` and add methods `decode` and `encode` to them.

It also allows manually specifying the field id, to allow evolving the schema in a compatible
way.

    @ThriftStruct
    case class Model(
      @ThriftField(1) a: Long,
      @ThriftField(2) list: Seq[Long] = List(2),
      @ThriftField(4) nestedMap: Map[Option[Map[Boolean, Int]], Option[Boolean]],
      otherValue: Seq[Int] = Nil
    )

The generated code for `encode`/`decode` is almost the same as scrooge-generated code, with the
exception of using `immutable.{Map, Seq, Set}` instead of `scala.collection.{Map, Seq, Set}`

An example of generated code printed with `-Ymacro-debug-lite` can be found in:
https://gitlab.com/ntnj/scrooge-macro/tree/master/scrooge-annotest/src/main/scala/com/twitter/scrooge/annotations/examples/Example1.scala

This will generate methods that will only encode/decode the fields annotated with `ThriftField`,
so it's necessary to specify a default value for the fields not annotated with `ThriftField`.
If `@ThriftField` is not assigned to any field, then they will be automatically assigned ids.

    val transport = new TMemoryBuffer(10000)
    val prot = new TJsonProtocol(transport)

    val abc = ABC(34, Set(45L, 65L), Map(23 -> Option(true), 12 -> None))

    abc.write(prot)
    ABC.decode(prot) == abc

## Implementation

This is implemented in these two files:

https://gitlab.com/ntnj/scrooge-macro/tree/master/scrooge-annotations/src/main/scala/com/twitter/scrooge/annotations/impl/StructImpl.scala

https://gitlab.com/ntnj/scrooge-macro/blob/struct/scrooge-annotations/src/main/scala/com/twitter/scrooge/annotations/impl/Thrift.scala

## Issues

1. These only define the `encode`/`decode`/`write` methods. Classes generated by scrooge have some
more methods describing the fields.
2. Matching is done on string name of types, so e.g. using `scala.Int` instead of `Int` will fail.
3. Only eager decoding is defined. Scrooge also creates a way to lazily decode the value
4. Pass-through fields are not supported.
5. Nested structs and unions and struct inheritance and thrift services are not supported.
6. There is a possible bug when deep-nesting container classes, the generated code may fail to
to compile. I saw one example when playing around, but don't have it now.
7. To use this end-to-end, thrift services have to modified to use the classes instead of
scrooge-generate classes.

## Status

This project is not going to be developed any further. I just created it to play
with scala macros.

I got the idea of defining ThriftStruct's in code from:
https://github.com/facebook/swift/blob/master/swift-codec/README.md
