package com.twitter.scrooge.annotations.examples

object Example1 {
  /*
  scala> :paste
  // Entering paste mode (ctrl-D to finish)

  import com.twitter.scrooge.annotations._

  @ThriftStruct
  case class ABC(a: Int, b: Set[Long], c: Map[Short, Option[Boolean]])


  // Exiting paste mode, now interpreting.

  performing macro expansion new ThriftStruct().macroTransform(case class ABC extends scala.Product with scala.Serializable {
    <caseaccessor> <paramaccessor> val a: Int = _;
      <caseaccessor> <paramaccessor> val b: Set[Long] = _;
        <caseaccessor> <paramaccessor> val c: Map[Short, Option[Boolean]] = _;
          def <init>(a: Int, b: Set[Long], c: Map[Short, Option[Boolean]]) = {
            super.<init>();
            ()
            }
            }) at source-<console>,line-12,offset=132
              {
              */
              case class ABC extends com.twitter.scrooge.ThriftStruct with scala.Product with scala.Serializable {
                <caseaccessor> <paramaccessor> val a: Int = _;
                  <caseaccessor> <paramaccessor> val b: Set[Long] = _;
                    <caseaccessor> <paramaccessor> val c: Map[Short, Option[Boolean]] = _;
                      def <init>(a: Int, b: Set[Long], c: Map[Short, Option[Boolean]]) = {
                        super.<init>();
                        ()
                        };
                        import org.apache.thrift.protocol._;
                        override def write(_oprot: TProtocol): Unit = {
                        _oprot.writeStructBegin(new TStruct("ABC"));
                        {
                          _oprot.writeFieldBegin(new TField("a", 8, 1));
                          _oprot.writeI32(a);
                          _oprot.writeFieldEnd()
                        };
                        {
                          _oprot.writeFieldBegin(new TField("b", 14, 2));
                          {
                            _oprot.writeSetBegin(new TSet(10, b.size));
                            b.foreach(((element) => _oprot.writeI64(element)));
                            _oprot.writeSetEnd()
                          };
                          _oprot.writeFieldEnd()
                        };
                        {
                          _oprot.writeFieldBegin(new TField("c", 13, 3));
                          {
                            _oprot.writeMapBegin(new TMap(6, 2, c.size));
                            c.foreach(<empty> match {
                            case scala.Tuple2((key @ _), (value @ _)) => {
                            _oprot.writeI16(key);
                            if (value.isDefined)
                              _oprot.writeBool(value.get)
                            else
                              ()
                          }
                            });
                            _oprot.writeMapEnd()
                            };
                            _oprot.writeFieldEnd()
                            };
                            _oprot.writeFieldStop();
                            _oprot.writeStructEnd()
                            }
                            };
                            object ABC extends com.twitter.scrooge.ThriftStructCodec3[ABC] {
                            def <init>() = {
                            super.<init>();
                            ()
                            };

                            import com.twitter.scrooge._;
                            import org.apache.thrift.protocol._;
                            override def encode(_item: ABC, _oproto: TProtocol): Unit = _item.write(_oproto);
                            override def decode(_iprot: TProtocol): ABC = _iprot match {
                            case (_iprot @ (_: LazyTProtocol)) => throw new Exception("LazyDecode not implemented yet")
                            case (_iprot @ _) => {
                            var a: Int = 0;
                            var b: Set[Long] = Set[Long]();
                            var c: Map[Short, scala.Option[Boolean]] = Map[Short, scala.Option[Boolean]]();
                            var _done = false;
                            _iprot.readStructBegin();
                            while$1(){
                              if (_done.unary_$bang)
                              {
                                {
                                  val _field = _iprot.readFieldBegin();
                                  if (_field.`type`.$eq$eq(TType.STOP))
                                    _done = true
                                  else
                                  {
                                    _field.id match {
                                      case 1 => _field.`type` match {
                                        case 8 => a = _iprot.readI32()
                                        case (_actualType @ _) => {
                                          val _expectedType = 8;
                                          throw new TProtocolException("Received wrong type for field \'name\' (expected=%s, actual=%s).".format(ttypeToString(_expectedType), ttypeToString(_actualType)))
                                        }
                                      }
                                      case 2 => _field.`type` match {
                                        case 14 => b = {
                                          import scala.collection.mutable;
                                          val _set = _iprot.readSetBegin();
                                          if (_set.size.$eq$eq(0))
                                          {
                                            _iprot.readSetEnd();
                                            Set.empty[Long]
                                          }
                                          else
                                          {
                                            val _rv = new mutable.HashSet[Long]();
                                            var _i = 0;
                                            while$2(){
                                              if (_i.$less(_set.size))
                                              {
                                                {
                                                  _rv.$plus$eq(_iprot.readI64());
                                                  _i.$plus$eq(1)
                                                };
                                                while$2()
                                              }
                                              else
                                                ()
                                            };
                                            _iprot.readSetEnd();
                                            _rv.toSet
                                          }
                                        }
                                        case (_actualType @ _) => {
                                          val _expectedType = 14;
                                          throw new TProtocolException("Received wrong type for field \'name\' (expected=%s, actual=%s).".format(ttypeToString(_expectedType), ttypeToString(_actualType)))
                                        }
                                      }
                                      case 3 => _field.`type` match {
                                        case 13 => c = {
                                          import scala.collection.mutable;
                                          val _map = _iprot.readMapBegin();
                                          if (_map.size.$eq$eq(0))
                                          {
                                            _iprot.readMapEnd();
                                            Map.empty[Short, scala.Option[Boolean]]
                                          }
                                          else
                                          {
                                            val _rv = new mutable.HashMap[Short, scala.Option[Boolean]]();
                                            var _i = 0;
                                            while$3(){
                                              if (_i.$less(_map.size))
                                              {
                                                {
                                                  val _key = _iprot.readI16();
                                                  val _value = scala.Some(_iprot.readBool());
                                                  _rv.update(_key, _value);
                                                  _i.$plus$eq(1)
                                                };
                                                while$3()
                                              }
                                              else
                                                ()
                                            };
                                            _iprot.readMapEnd();
                                            _rv.toMap
                                          }
                                        }
                                        case (_actualType @ _) => {
                                          val _expectedType = 13;
                                          throw new TProtocolException("Received wrong type for field \'name\' (expected=%s, actual=%s).".format(ttypeToString(_expectedType), ttypeToString(_actualType)))
                                        }
                                      }
                                      case _ => ()
                                    };
                                    _iprot.readFieldEnd()
                                  }
                                };
                                while$1()
                              }
                              else
                                ()
                            };
                            _iprot.readStructEnd();
                            new ABC(a = a, b = b, c = c)
                          }
                            }
}
                          }