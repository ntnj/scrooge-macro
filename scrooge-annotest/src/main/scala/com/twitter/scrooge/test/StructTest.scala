package com.twitter.scrooge.test

import com.twitter.scrooge.annotations.{ThriftField, ThriftStruct}

object StructTest {
  @ThriftStruct
  case class ABC(a: Int, b: Option[Boolean], m: Map[Int, Boolean], set: Set[Long])

  @ThriftStruct
  case class A(a: Int, b: Boolean, c: Map[Option[Int], Map[Option[Map[Boolean, Int]], Option[Boolean]]])

  @ThriftStruct
  case class B(@ThriftField(3) d: Seq[Double], f: Seq[Option[String]] = Nil, @ThriftField(5) g: Set[Option[Byte]])
}
