package com.twitter.scrooge.annotations.impl

import scala.reflect.macros.whitebox

class StructImpl(val c: whitebox.Context) {
  import c.universe._
  val thrift = new Thrift[c.type](c)

  case class Field(id: Int, name: TermName, typ: Tree, defaultValue: Option[Tree] = None) {
    val ttype = thrift.getTType(typ)
    def initVar = q"var $name: ${ttype.scalaType} = ${defaultValue.getOrElse(ttype.default)}"
    def decodeCase(field: TermName, prot: TermName): Tree = {
      //TODO: Figure out a way to substitute name with actual field name in exception message
      cq"""
        $id => $field.`type` match {
          case ${ttype.thriftType} => $name = ${ttype.read(prot)}
          case _actualType =>
            val _expectedType = ${ttype.thriftType}
            throw new TProtocolException(
              "Received wrong type for field 'name' (expected=%s, actual=%s).".format(
                ttypeToString(_expectedType),
                ttypeToString(_actualType)
              )
            )
        }
        """
    }
    def writeField(prot: TermName) = {
      q"""
       $prot.writeFieldBegin(new TField(${name.toString}, ${ttype.thriftType}, $id))
       ${ttype.write(prot, q"$name")}
       $prot.writeFieldEnd()
       """
    }
  }

  def eagerDecode(className: TypeName, fields: Seq[Field])(prot: TermName) = {
    val field = TermName("_field")
    val done = TermName("_done")
    q"""
     ..${fields.map(_.initVar)}
     var $done = false

     $prot.readStructBegin()
     while (! $done) {
       val $field = $prot.readFieldBegin()
       if ($field.`type` == TType.STOP) {
         $done = true
       } else {
         $field.id match {
           case ..${fields.map(_.decodeCase(field, prot))}
           case _ =>
         }
         $prot.readFieldEnd()
       }
     }
     $prot.readStructEnd()
     new $className(..${fields.map(f => q"${f.name} = ${f.name}")})
     """
  }

  def lazyDecode(className: TypeName, fields: Seq[Field])(prot: TermName) = {
    q"""throw new Exception("LazyDecode not implemented yet")"""
  }

  def decodeMethod(className: TypeName, fields: Seq[Field]) = {
    val prot = TermName("_iprot")
    //TODO: Add support for lazy decoding
    q"""
     override def decode($prot: TProtocol): $className =
       $prot match {
         case $prot: LazyTProtocol => ${lazyDecode(className, fields)(prot)}
         case $prot => ${eagerDecode(className, fields)(prot)}
       }
     """
  }

  def encodeMethod(className: TypeName) = {
    q"""
     override def encode(_item: $className, _oproto: TProtocol): Unit = {
       _item.write(_oproto)
     }
     """
  }

  def writeMethod(className: TypeName, fields: Seq[Field]) = {
    val prot = TermName("_oprot")
    q"""
     override def write($prot: TProtocol): Unit = {
       $prot.writeStructBegin(new TStruct(${className.toString}))
       ..${fields.map(_.writeField(prot))}
       $prot.writeFieldStop()
       $prot.writeStructEnd()
     }
     """
  }

  def populateMethods(classDef: ClassDef, moduleDef: ModuleDef) = {
    val className = classDef.name

    val fields = {
      val q"$mods class $tpname[..$tparams] $ctorMods(...$paramss) extends { ..$earlydefns } with ..$parents { $self => ..$stats }" = classDef
      val list = paramss.head
      val assignedIds = list.flatMap(_.mods.annotations).exists { case q"new $ann(..$args)" =>
        ann match {
          case tq"ThriftField" => true
          case _ => false
        }
      }
      list.zipWithIndex.flatMap { case (valDef: ValDef, i) =>
        val idOpt = if (assignedIds) {
          valDef.mods.annotations.collectFirst {
            case q"new ThriftField(${id: Int})" => id
          }
        } else {
          Some(i + 1)
        }
        val default = Some(valDef.rhs).filter(_ != EmptyTree)
        if (default.isEmpty && idOpt.isEmpty)
          c.abort(valDef.pos, "Non ThriftField's should have default values")
        idOpt.map { id =>
          Field(id, valDef.name, valDef.tpt, default)
        }
      }
    }
    val updatedClassDef = {
      val q"$mods class $tpname[..$tparams] $ctorMods(...$paramss) extends { ..$earlydefns } with ..$parents { $self => ..$stats }" = classDef
      val updatedParents = parents :+ tq"com.twitter.scrooge.ThriftStruct"
      q"""
       $mods class $tpname[..$tparams] $ctorMods(...$paramss) extends { ..$earlydefns } with ..$updatedParents { $self =>
         ..$stats
         import org.apache.thrift.protocol._
         ${writeMethod(className, fields)}
       }
       """
    }
    val updatedModuleDef = {
      val q"$mods object $tname extends { ..$earlydefns } with ..$parents { $self => ..$body }" = moduleDef
      val updatedParents = List(tq"com.twitter.scrooge.ThriftStructCodec3[$className]")
      q"""
       $mods object $tname extends { ..$earlydefns } with ..$updatedParents { $self =>
         ..$body
         import com.twitter.scrooge._
         import org.apache.thrift.protocol._
         ${encodeMethod(className)}
         ${decodeMethod(className, fields)}
       }
       """
    }

    q"""
     $updatedClassDef
     $updatedModuleDef
     """
  }

  def impl(annottees: c.Expr[Any]*) = {
    annottees map (_.tree) match {
      case (classDef: ClassDef) :: Nil if classDef.mods.hasFlag(Flag.CASE)=>
        val TypeName(className) = classDef.name
        populateMethods(classDef, q"object ${TermName(className)}")
      case (classDef: ClassDef) :: (moduleDef: ModuleDef) :: Nil if classDef.mods.hasFlag(Flag.CASE) =>
        val objParents = moduleDef.impl.parents
        objParents match {
          case Nil | List(tq"scala.AnyRef") =>
            populateMethods(classDef, moduleDef)
          case _ =>
            //TODO: This position may be relaxed to objects only extending traits
            //Sadly, ThriftStructCodec3 is a class instead of a trait
            c.abort(c.enclosingPosition, "Objects cannot extend anything, as they need to extend ThriftStructCodec3")
        }
      case _ => c.abort(c.enclosingPosition, "Invalid annotation target: must be a case class")
    }
  }
}
