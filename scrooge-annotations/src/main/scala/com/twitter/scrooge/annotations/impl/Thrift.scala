package com.twitter.scrooge.annotations.impl

import org.apache.thrift.protocol.TType
import scala.reflect.macros.whitebox

class Thrift[C <: whitebox.Context](val c: C) {
  import c.universe._

  def getTType(tree: Tree): Type = tree match {
    //TODO: This matches on String repr of type (Is there a way to use canonical type?)
    case tq"Option[$t]" => Option(getTType(t))
    case tq"Boolean" => Bool
    case tq"Byte" => Byte
    case tq"Double" => Double
    case tq"Short" => Short
    case tq"Int" => Int
    case tq"Long" => Long
    case tq"String" => String
    //TODO: Implement Struct if possible for nested values
    case tq"Map[$k, $v]" => Map(getTType(k), getTType(v))
    case tq"Set[$t]" => Set(getTType(t))
    case tq"Seq[$t]" => Seq(getTType(t))
    case _ => c.abort(tree.pos, "This type is not currently supported in thrift")
  }

  trait Type {
    def scalaType: Tree
    def thriftType: scala.Byte
    def default: Tree
    def read(prot: TermName): Tree
    def write(prot: TermName, value: Tree): Tree
  }

  case class Option(t: Type) extends Type {
    def scalaType = tq"scala.Option[${t.scalaType}]"
    def thriftType = t.thriftType
    def default = q"scala.None"
    def read(prot: TermName) = q"scala.Some(${t.read(prot)})"
    def write(prot: TermName, value: Tree) = q"if ($value.isDefined) ${t.write(prot, q"$value.get")}"
  }

  object Bool extends Type {
    def scalaType = tq"Boolean"
    def thriftType = TType.BOOL
    def default = q"false"
    def read(prot: TermName) = q"$prot.readBool()"
    def write(prot: TermName, value: Tree) = q"$prot.writeBool($value)"
  }

  object Byte extends Type {
    def scalaType = tq"Byte"
    def thriftType = TType.BYTE
    def default = q"0"
    def read(prot: TermName) = q"$prot.readByte()"
    def write(prot: TermName, value: Tree) = q"$prot.writeByte($value)"
  }

  object Double extends Type {
    def scalaType = tq"Double"
    def thriftType = TType.DOUBLE
    def default = q"0.0"
    def read(prot: TermName) = q"$prot.readDouble()"
    def write(prot: TermName, value: Tree) = q"$prot.writeDouble($value)"
  }

  object Short extends Type {
    def scalaType = tq"Short"
    def thriftType = TType.I16
    def default = q"0"
    def read(prot: TermName) = q"$prot.readI16()"
    def write(prot: TermName, value: Tree) = q"$prot.writeI16($value)"
  }

  object Int extends Type {
    def scalaType = tq"Int"
    def thriftType = TType.I32
    def default = q"0"
    def read(prot: TermName) = q"$prot.readI32()"
    def write(prot: TermName, value: Tree) = q"$prot.writeI32($value)"
  }

  object Long extends Type {
    def scalaType = tq"Long"
    def thriftType = TType.I64
    def default = q"0L"
    def read(prot: TermName) = q"$prot.readI64()"
    def write(prot: TermName, value: Tree) = q"$prot.writeI64($value)"
  }

  object String extends Type {
    def scalaType = tq"String"
    def thriftType = TType.STRING
    def default = q"${""}"
    def read(prot: TermName) = q"$prot.readString()"
    def write(prot: TermName, value: Tree) = q"$prot.writeString($value)"
  }

  case class Struct(name: TypeName, members: scala.collection.immutable.Seq[Type]) extends Type {
    def scalaType = tq"$name"
    def thriftType = TType.STRUCT
    def default = q"null"
    def read(prot: TermName) = q"${name.toString}.decode($prot)"
    def write(prot: TermName, value: Tree) = q"$value.write($prot)"
  }

  case class Map(k: Type, v: Type) extends Type {
    def scalaType = tq"Map[${k.scalaType}, ${v.scalaType}]"
    def thriftType = TType.MAP
    def default = q"Map[${k.scalaType}, ${v.scalaType}]()"
    def read(prot: TermName) = {
      q"""
       import scala.collection.mutable
       val _map = $prot.readMapBegin()
       if (_map.size == 0) {
         $prot.readMapEnd()
         Map.empty[${k.scalaType}, ${v.scalaType}]
       } else {
         val _rv = new mutable.HashMap[${k.scalaType}, ${v.scalaType}]
         var _i = 0
         while (_i < _map.size) {
           val _key = ${k.read(prot)}
           val _value = ${v.read(prot)}
           _rv(_key) = _value
           _i += 1
         }
         $prot.readMapEnd()
         _rv.toMap
       }
       """
    }
    def write(prot: TermName, value: Tree) = {
      q"""
       $prot.writeMapBegin(new TMap(${k.thriftType}, ${v.thriftType}, $value.size))
       $value.foreach { case (key, value) =>
         ${k.write(prot, q"key")}
         ${v.write(prot, q"value")}
       }
       $prot.writeMapEnd()
       """
    }
  }

  case class Set(t: Type) extends Type {
    def scalaType = tq"Set[${t.scalaType}]"
    def thriftType = TType.SET
    def default = q"Set[${t.scalaType}]()"
    def read(prot: TermName) = {
      q"""
       import scala.collection.mutable
       val _set = $prot.readSetBegin()
       if (_set.size == 0) {
         $prot.readSetEnd()
         Set.empty[${t.scalaType}]
       } else {
         val _rv = new mutable.HashSet[${t.scalaType}]
         var _i = 0
         while (_i < _set.size) {
           _rv += ${t.read(prot)}
           _i += 1
         }
         $prot.readSetEnd()
         _rv.toSet
       }
       """
    }
    def write(prot: TermName, value: Tree) = {
      q"""
       $prot.writeSetBegin(new TSet(${t.thriftType}, $value.size))
       $value.foreach { element =>
         ${t.write(prot, q"element")}
       }
       $prot.writeSetEnd()
       """
    }
  }

  case class Seq(t: Type) extends Type {
    def scalaType = tq"Seq[${t.scalaType}]"
    def thriftType = TType.LIST
    def default = q"Seq[${t.scalaType}]()"
    def read(prot: TermName) = {
      q"""
       import scala.collection.mutable
       val _list = $prot.readListBegin()
       if (_list.size == 0) {
         $prot.readListEnd()
         Nil
       } else {
         val _rv = new mutable.ArrayBuffer[${t.scalaType}](_list.size)
         var _i = 0
         while (_i < _list.size) {
           _rv += ${t.read(prot)}
           _i += 1
         }
         $prot.readListEnd()
         _rv.toSeq
       }
       """
    }
    def write(prot: TermName, value: Tree) = {
      q"""
       $prot.writeListBegin(new TList(${t.thriftType}, $value.size))
       $value match {
         case _: IndexedSeq[_] =>
           var _i = 0
           var _size = $value.size
           while (_i < _size) {
             val element = $value(_i)
             ${t.write(prot, q"element")}
             _i += 1
           }
         case _ =>
           $value.foreach { element =>
             ${t.write(prot, q"element")}
           }
       }
       $prot.writeListEnd()
       """
    }
  }

  //TODO: Implement Enum if possible
}
