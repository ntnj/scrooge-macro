package com.twitter.scrooge.annotations

import com.twitter.scrooge.annotations.impl.StructImpl
import scala.annotation.{compileTimeOnly, StaticAnnotation}
import scala.language.experimental.macros

@compileTimeOnly("enable macro paradise plugin to use this annotation")
class ThriftStruct extends StaticAnnotation {
  def macroTransform(annottees: Any*): Any = macro StructImpl.impl
}

case class ThriftField(id: Int) extends StaticAnnotation